import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <h1>Some Angular elements brief explanation</h1>
    <dl>
      <dt>Component:</dt>
        <dd>
          The component controls the display of the view on the screen.
          Components are the most basic building block of an UI in an Angular application.
        </dd>
      <dt>Services:</dt>
        <dd>
          Services in Angular represent a fairly wide range of classes, which perform some specific tasks width data, logings etc.
          Services do not work with html markup, they perform a strictly defined and rather narrow task.
        </dd>
      <dt>Routing:</dt>
        <dd>
          Routing allows to navigate from one view to another according users actions.
        </dd>
      <dt>Data binding:</dt>
        <dd>
          Data binding allows different parts of the template bind to certain values of a component.
        </dd>
      <dt>Observables:</dt>
        <dd>
          Observables are a powerful way to manage asynchronous data flows.
          Each http service method returns an observable of http response objects.
          An observable is a stream of events that you can process with array-like operators.
        </dd>
      <dt>FormsModule module:</dt>
        <dd>
          FormsModule is a Angular ng module which allows you to work with forms.
        </dd>
      <dt>Directives:</dt>
        <dd>
          The directives define the set of instructions that are used when rendering the html code.
          It represents a class with directive metadata.
          <br><span>There are three kinds of directives in Angular:</span>
          <ul>
            <li>Components</li>
            <li>Structural directives</li>
            <li>Attribute directives</li>
          </ul>
        </dd>
    </dl>
  `,
  styles: [`
    h1 {
      text-align: center;
    }

    dl {
      width: 70%;
      margin: auto;
      background-color: #F7F7FA;
      border: 1px solid #CCCCCF;
      border-radius: 5px;
      padding: 20px;
    }

    dl dt {
      font-weight: bold;
    }

    dl dd {
      margin-top: 5px;
      margin-bottom: 20px;
      text-align: justify;
    }
  `]
})
export class AppComponent {
  title = 'app';
}
